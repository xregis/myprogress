package com.myprogress.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.myprogress.app.email.MainEmailActivity;


public class MainActivity extends ActionBarActivity {

    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void OnBtnProgress(View view) {
        Context context = MainActivity.this;
        showProgress(context);
    }

    private void showProgress(Context context) {
        // Erro - getApplicationContext() / getBaseContext() / getApplication()
        pd = new ProgressDialog(context);
        pd.setIndeterminate(true);
        pd.setCancelable(true);
        pd.setMessage("Em progresso...");
        pd.setTitle("Aguarde");
        pd.show();
    }

    public void OnBtnFragA(View view) {
        Fragment fr = new FragmentA();

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_place, fr);
        fragmentTransaction.commit();
    }

    public void OnBtnMain2(View view) {
        Intent intent = new Intent(getApplicationContext(), MainActivity2.class);
        startActivity(intent);
    }

    public void OnBtnMain3(View view) {
        Intent intent = new Intent(getApplicationContext(), MainActivity3.class);
        startActivity(intent);
    }

    public void OnBtnMain4(View view) {
        Intent intent = new Intent(getApplicationContext(), MainActivityTab4.class);
        startActivity(intent);
    }

    public void OnBtnMain5(View view) {
        Intent intent = new Intent(getApplicationContext(), MainActivity5TabPage.class);
        startActivity(intent);
    }

    public void OnbtnDialogLogin(View view) {
        Intent intent = new Intent(getApplicationContext(), AndroidDialog.class);
        startActivity(intent);
    }

    public void OnbtnSendEmail(View view) {
        Intent intent = new Intent(getApplicationContext(), MainEmailActivity.class);
        startActivity(intent);
    }
}
