package com.myprogress.app;



import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 *
 */
public class FragmentB extends Fragment {


    private ProgressDialog pd;

    public FragmentB() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_fragment_b, container, false);
        Button btnProgress = (Button) rootView.findViewById(R.id.btnProgressB);
        btnProgress.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgress(getActivity());
            }
        });
        return rootView;
    }


    private void showProgress(Context context) {
        // getApplicationContext() getApplication

        pd = new ProgressDialog(this.getActivity());
        pd.setIndeterminate(true);
        pd.setCancelable(true);
        pd.setMessage("Em progresso B...");
        pd.setTitle("Aguarde");
        pd.show();
    }
}
