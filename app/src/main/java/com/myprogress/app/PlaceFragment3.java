package com.myprogress.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by rbsilva on 16/05/2014.
 */
public class PlaceFragment3 extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceFragment3 newInstance(int sectionNumber) {

        PlaceFragment3 fragment = new PlaceFragment3();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PlaceFragment3() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_c, container, false);
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        int i = getArguments().getInt(ARG_SECTION_NUMBER);
        if (activity.getClass().getSimpleName().contains("MainActivity3"))
            ((MainActivity3) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
        Toast.makeText(getActivity(), "Fragment " + i, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_frag3, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
}