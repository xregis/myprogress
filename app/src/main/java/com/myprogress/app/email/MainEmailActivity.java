package com.myprogress.app.email;

import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.myprogress.app.MainActivity;
import com.myprogress.app.R;

import java.util.logging.Handler;

public class MainEmailActivity extends ActionBarActivity {

    private EditText emailEdit;
    private EditText subjectEdit;
    private static EditText messageEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_email);

        emailEdit = (EditText) findViewById(R.id.email);
        subjectEdit = (EditText) findViewById(R.id.subject);
        messageEdit = (EditText) findViewById(R.id.message);
        Button sendButton = (Button) findViewById(R.id.send);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailEdit.getText().toString();
                String subject = subjectEdit.getText().toString();
                String message = messageEdit.getText().toString();
                SendEmail sendEmail = new SendEmail(MainEmailActivity.this);
                sendEmail.sendMail(email, subject, message);
            }
        });
    }

    static public enum WhatAbout { ART, LIFE, MONEY, WORLD }
    static public WhatAbout[] wa = WhatAbout.values();

    static android.os.Handler updater1 = new android.os.Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what < wa.length) {
                String header = "[H:1,Th:" + Thread.currentThread().getId() + "] ";
                switch (wa[msg.what]) {
                    case ART:
                        messageEdit.append(header + "ART is Art.\n");
                        break;
                    case LIFE:
                        messageEdit.append(header + "LIFE is Life.\n");
                        break;
                    case MONEY:
                        messageEdit.append(header + "MONEY is Money.\n");
                        break;
                    case WORLD:
                        messageEdit.append(header + "WORLD is World.\n");
                        break;
                }
                if (msg.what+1 < wa.length)
                    sendMessageDelayed(obtainMessage(msg.what+1, 0, 0), 2000);
            }
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_email, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
