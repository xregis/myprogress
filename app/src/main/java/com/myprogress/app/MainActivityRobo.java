package com.myprogress.app;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;


public class MainActivityRobo extends RoboActivity {

    @InjectView(R.id.tvRobo) TextView name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity_robo);

        name.setText("Roboguice");
    }
}
