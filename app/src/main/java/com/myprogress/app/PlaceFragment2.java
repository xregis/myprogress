package com.myprogress.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by rbsilva on 16/05/2014.
 */
public class PlaceFragment2 extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    private ProgressDialog pd;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceFragment2 newInstance(int sectionNumber) {

        PlaceFragment2 fragment = new PlaceFragment2();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PlaceFragment2() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_b, container, false);
        setHasOptionsMenu(true);

        Button btnProgress = (Button) rootView.findViewById(R.id.btnProgress);
        btnProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgress();
                demora();
            }


        });
        return rootView;
    }

    private void demora() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                pd.dismiss();
            }
        }).start();
    }

    private void showProgress() {
        pd = new ProgressDialog(getActivity());
        pd.setIndeterminate(true);
        pd.setCancelable(true);
        pd.setMessage("Em progresso...");
        pd.setTitle("Aguarde");
        pd.show();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        int i = getArguments().getInt(ARG_SECTION_NUMBER);
        if (activity.getClass().getSimpleName().contains("MainActivity3"))
            ((MainActivity3) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
        Toast.makeText(getActivity(), "Fragment " + i, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_frag2, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
}