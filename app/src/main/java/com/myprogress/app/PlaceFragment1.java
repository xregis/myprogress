package com.myprogress.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rbsilva on 16/05/2014.
 */
public class PlaceFragment1 extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceFragment1 newInstance(int sectionNumber) {

        PlaceFragment1 fragment = new PlaceFragment1();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PlaceFragment1() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_a, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);

        ListView listView = (ListView) rootView.findViewById(R.id.list);
        List<String> arrayList = new ArrayList<String>();
        for (int i = 1; i < 500; i++)
            arrayList.add("Valor " + i);

        String[] values = arrayList.toArray(new String[] {});
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, values);

        // Assign adapter to ListView
        listView.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        int i = getArguments().getInt(ARG_SECTION_NUMBER);
        if (activity.getClass().getSimpleName().contains("MainActivity3"))
            ((MainActivity3) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));

        Toast.makeText(getActivity(), activity.getClass().getSimpleName() + " - Fragment " + i, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_frag1, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
}